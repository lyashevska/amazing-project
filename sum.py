def sum(a : float, b : float) -> float:
    """A sum function of two floats."""
    return a + b

def sum(a : int, b : int) -> int:
    """A sum function of two int."""
    return a + b
